# honye

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### diff

1. vue2.0 全量对比的
2. vue3.0 新增了静态标记

### 静态提升

1. 2.0 不论元素是否参与更新，每次都会重新创建，再渲染
2. 3.0 对于不参与更新的元素，会做一个静态的提升，只会创建一次，渲染时重新使用

### 事件缓存
